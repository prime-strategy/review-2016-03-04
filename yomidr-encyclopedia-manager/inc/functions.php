<?php
/**
 * 記者記事情報を取得する
 *
 * @access public
 * @param $encyclopedia_id
 * @return object|false 記者記事情報 
 */
function get_encyclopedia( $encyclopedia_id ) {
	global $yomiDr_Encyclopedia_Manager;
	return $yomiDr_Encyclopedia_Manager->get_encyclopedia( $encyclopedia_id );
}

/**
 * 記者記事情報を病名スラッグから取得する
 *
 * @access public
 * @param $disease_slug
 * @return object|false 記者記事情報 
 */
function get_encyclopedia_by_disease( $disease_slug ) {
	global $yomiDr_Encyclopedia_Manager;
	return $yomiDr_Encyclopedia_Manager->get_encyclopedia_by_disease( $disease_slug );
}

/**
 * 法研情報を取得する
 *
 * @access public
 * @param $houken_id
 * @return object|false 法研情報 
 */
function get_houken( $houken_id ) {
	global $yomiDr_Encyclopedia_Manager;
	return $yomiDr_Encyclopedia_Manager->get_houken( $houken_id );
}

/**
 * 法研情報を病名スラッグから取得する
 *
 * @access public
 * @param $disease_slug
 * @return object|false 法研情報 
 */
function get_houken_by_disease( $disease_slug ) {
	global $yomiDr_Encyclopedia_Manager;
	return $yomiDr_Encyclopedia_Manager->get_houken_by_disease( $disease_slug );
}

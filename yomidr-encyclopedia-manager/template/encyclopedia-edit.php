<?php if ( ! defined( 'ABSPATH' ) ) { exit; }
global $wpdb;
$mode = 'edit';
if ( isset( $_GET['encyclopedia'] ) && $_GET['encyclopedia'] ) {
	$data = $this->get_encyclopedia( $_GET['encyclopedia'] );
	if ( ! $data ) {
		wp_die( '該当の記者記事が見つかりませんでした。記者記事一覧より再選択してください。' );
	}
	$mode = 'edit';
} else {
	if ( isset( $_POST['add_encyclopedia'] ) ) {
		$data = $_POST;
	} else {
		$data = false;
	}
	$mode = 'add';
}
if ( 'edit' == $mode ) {
	$nonce = wp_create_nonce( 'delete-encyclopedia' );
	$delete_link = add_query_arg( array( 'action' => 'delete', 'encyclopedia' => $_GET['encyclopedia'], 'nonce' => $nonce ), admin_url( 'admin.php?page=yomidr-encyclopedia-manager%2Fencyclopedia-manager.php' ) );
} ?>
<div class="wrap">
	<?php if ( 'add' == $mode ) : ?>
		<h1>記者記事の新規追加</h1>
	<?php elseif ( 'edit' == $mode ) : ?>
		<h1>記者記事の編集</h1>
	<?php endif; ?>
<?php
$class = ( isset( $_REQUEST['error'] ) ) ? 'error' : 'updated';
if ( isset( $_GET['message'] ) ) {
	switch ( $_GET['message'] ) {
		case 1 :
			$message = '記者記事を公開しました。';
			break;
		case 3 :
			$message = '記者記事を更新しました。';
			break;
		default :
			$message = '';
	}
}
if ( isset( $message ) && $message ) : ?>
	<div id="message" class="<?php echo $class; ?> notice is-dismissible"><p><?php echo $message; ?></p></div>
	<?php $_SERVER['REQUEST_URI'] = remove_query_arg( array( 'message', 'error' ), $_SERVER['REQUEST_URI'] );
endif; ?>
	<div id="ajax-response"></div>
	<ul id="errorList" class="notice"></ul>
	<form method="post" action="" id="form-encyclopedia" class="validate">
		<?php wp_nonce_field( $mode . '-encyclopedia', $mode . '-nonce' ); ?>
		<?php if ( 'edit' == $mode ) : ?>
			<p class="alignright"><a id="delete" class="button button-primary" href="<?php echo esc_url( $delete_link ); ?>">削除</a></p>
		<?php endif; ?>
		<table class="form-table">
			<tbody>
				<tr class="form-field form-required term-status-wrap">
					<th scope="row">ステータス<span class="required">*</span></th>
					<td>
						<label><input type="radio" name="encyclopedia_status" value="1"<?php if ( ( 'add' == $mode ) || ( isset( $data ) && $data && 1 == $data->encyclopedia_status ) ) { echo ' checked="checked"'; } ?>>公開</label>
						<label><input type="radio" name="encyclopedia_status" value="0"<?php if ( isset( $data ) && $data && 0 == $data->encyclopedia_status ) { echo ' checked="checked"'; } ?>>非公開</label>
					</td>
				</tr>
				<tr class="form-field form-required term-disease-wrap">
					<th scope="row">病名<span class="required">*</span></th>
					<td>
						<select name="part" class="postform">
							<option value="-1" selected="selected">選択なし</option>
							<?php 
							$parts = get_parts();
							foreach ( $parts as $part ) { 
							?>
								<option value="<?php echo esc_attr( $part->part_slug ); ?>"><?php echo esc_html( $part->part_name ); ?></option>
							<?php } ?>
						</select>
						<p class="description">部位・テーマで病名を絞り込むことができます。</p>
						<ul class="radio-group">
							<?php
							// wp_encyclopedia テーブルから、記者記事に紐づいている disease_slug 配列を取得
							$ret = $wpdb->get_results( "SELECT disease_slug FROM {$this->encyclopedia_table}", OBJECT_K );
							foreach ( $ret as $key => $val ) {
								$disease_slugs_from_encyclopedia[] = $key;
							}
							// $disease_slugs_from_encyclopedia 配列から、自分自身と紐づいている病名スラッグを取り除く
							if ( isset( $data->disease_slug ) && $data->disease_slug ) {
								$index = array_search( $data->disease_slug, $disease_slugs_from_encyclopedia );
								if ( isset( $index ) ) {
									unset( $disease_slugs_from_encyclopedia[$index] );
								}
							}

							// 病名一覧を取得
							$args = array( 'hide_empty' => false );
							$diseases = get_terms( 'disease', $args );

							// 記者記事に紐づいていない病名のみを表示させる
							foreach ( $diseases as $disease ) :
								if ( ! in_array( $disease->slug, $disease_slugs_from_encyclopedia ) ) :

								// JS用の部位のクラスを作成する
								$part_ids = get_parts_id_by_disease_slug( $disease->slug );
								$part_class = array();
								foreach ( $part_ids as $part_id ) {
									$part = get_part( $part_id );
									if ( $part ) {
										$part_class[] = $part->part_slug;
									}
								}
									$part_class = implode( ' ', $part_class );
								?>
									<li class="<?php echo esc_attr( $part_class ); ?>">
										<label>
											<input type="radio" name="disease_slug" value="<?php echo esc_attr( $disease->slug ); ?>"<?php if ( isset( $data ) && $data && $disease->slug == $data->disease_slug ) { echo ' checked="checked"'; } ?>><span class="checked_disease_target"><?php echo esc_html( $disease->name ); ?></span></label>
									</li>
								<?php
									endif;
								endforeach; ?>
						</ul>
						<p id="checked_disease_text">現在選択されている病名：<span class="checked_disease_text_target">なし</span></p>
					</td>
				</tr>
			</tbody>
		</table>
		<p><label for="content">記事内容<span class="required">*</span></label></p>
		<p><textarea name="encyclopedia_content" id="content"><?php if ( isset( $data ) && $data ) { echo esc_html( $data->encyclopedia_content ); } ?></textarea></p>
		<p class="submit"><input type="submit" name="<?php echo $mode . '_encyclopedia'; ?>" id="submit" class="button button-primary" value="更新"></p>
	</form>
</div>

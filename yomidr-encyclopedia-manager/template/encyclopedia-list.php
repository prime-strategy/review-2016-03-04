<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
global $wpdb;

// 検索キーワードの取得
$search = isset( $_GET['s'] ) ? wp_unslash($_GET['s']) : '';
// ページ名の取得
$page_name = isset( $_GET['page'] ) ? wp_unslash($_GET['page']) : '';
// 1ページあたりの表示件数
$page_items = 10;
// 現在のページ番号
$paged = isset( $_GET['paged'] ) && (int)$_GET['paged'] ? (int)$_GET['paged'] : 1;
// SQLでの表示開始位置
$start = ( $paged - 1 ) * $page_items;

// SQLの作成
// 検索結果に応じて実行SQLを変更
// encyclopediaテーブルのdisease_slugがtermテーブルに存在しない場合、検索用と同じSQLにすると通常の一覧に表示されないため、SQL自体を変更
if ( $search ) {
	$sql = "
SELECT
 SQL_CALC_FOUND_ROWS
 e.encyclopedia_id,
 t.name,
 tm.meta_value,
 e.encyclopedia_content,
 e.encyclopedia_status,
 e.encyclopedia_date,
 e.encyclopedia_author
 FROM ({$this->encyclopedia_table} as e INNER JOIN {$wpdb->terms} as t ON e.disease_slug = t.slug)
 INNER JOIN {$wpdb->termmeta} as tm ON t.term_id = tm.term_id
 WHERE tm.meta_key = 'disease_kana' AND (t.name LIKE '%%%s%%' OR tm.meta_value LIKE '%%%s%%') LIMIT %d, %d;";
	$encyclopedias = $wpdb->get_results( $wpdb->prepare( $sql, $search, $search, $start, $page_items  ) );
} else {
	$sql = "
SELECT
 SQL_CALC_FOUND_ROWS
 encyclopedia_id,
 disease_slug,
 encyclopedia_content,
 encyclopedia_status,
 encyclopedia_date,
 encyclopedia_author
 FROM {$this->encyclopedia_table}
 LIMIT %d, %d;";
	$encyclopedias = $wpdb->get_results( $wpdb->prepare( $sql, $start, $page_items  ) );
}

// 取得結果に応じて変数を設定
if ( $encyclopedias ) {
	$found_rows = $wpdb->get_var( 'SELECT FOUND_ROWS()' );
	$max_pages = ceil( $found_rows / $page_items );
} else {
	$found_rows = 0;
	$max_pages = 0;
}
?>

<div class="wrap">
	<h1>記者記事 <a href="<?php echo esc_url( admin_url( 'admin.php?page=encyclopedia-manager.phpyomidr-encyclopedia-add' ) ); ?>" class="page-title-action">新規追加</a></h1>
	<form id="posts-filter" method="get">
		<input type="hidden" name="page" value="<?php echo esc_attr( $page_name ); ?>">
		<p class="search-box">
			<label class="screen-reader-text" for="post-search-input">記者記事を検索:</label>
			<input type="search" id="post-search-input" name="s" value="<?php echo esc_attr( $search ); ?>">
			<input type="submit" id="search-submit" class="button" value="記者記事を検索">
		</p>
	</form>
	<div class="tablenav top">
		<?php echo yomidr_admin_pager ( $paged, $max_pages, $found_rows ); ?>
		<br class="clear">
	</div>

	<table class="wp-list-table widefat fixed striped encyclopedia">
		<thead>
			<tr>
				<th scope="col" id="disease_name" class="manage-column column-name">名前（紐づいている病名）</th>
				<th scope="col" id="status" class="manage-column column-status">ステータス</th>
				<th scope="col" id="updated_date" class="manage-column column-updated_date">更新日</th>
				<th scope="col" id="author" class="manage-column column-author">更新者</th>
			</tr>
		</thead>
		<tbody id="the-list">
<?php
if ( $encyclopedias ) :
	foreach( $encyclopedias as $encyclopedia ) :
		$user = get_userdata( $encyclopedia->encyclopedia_author );
		if ( $user && $user->display_name ) {
			$author = $user->display_name;
		} else {
			$author = '削除済みユーザー';
		}

		// 通常時と検索時で病名の取得方法が異なる
		if ( $search ) {
			$disease_name = $encyclopedia->name;
		} else {
			$disease = get_term_by( 'slug', $encyclopedia->disease_slug, 'disease' );
			if ( $disease ) {
				$disease_name = $disease->name;
			} else {
				$disease_name = '（病名がありません）';
			}
		}
?>
			<tr id="encyclopedia-<?php echo esc_attr( $encyclopedia->encyclopedia_id ); ?>" class="iedit author-self level-0 encyclopedia-<?php echo esc_attr( $encyclopedia->encyclopedia_id ); ?> type-encyclopedia status-publish format-standard hentry">
				<td class="name column-name has-row-actions column-primary page-title" data-colname="名前（紐づいている病名）">
					<strong>
						<a class="row-title" title="“<?php echo esc_attr( $disease_name ); ?>”を編集する" href="<?php echo esc_url( admin_url( 'admin.php?page=yomidr-encyclopedia-manager%2Fencyclopedia-manager.php&encyclopedia='.esc_attr( $encyclopedia->encyclopedia_id ).'&action=edit' ) ); ?>"><?php echo esc_html( $disease_name ); ?></a>
					</strong>
				</td>
				<td class="status column-status" data-colname="ステータス"><?php echo esc_html( $encyclopedia->encyclopedia_status ? '公開' : '非公開' ); ?></td>
				<td class="date column-date" data-colname="更新日"><?php echo esc_html( date_i18n( get_option('date_format').' H:i', strtotime( $encyclopedia->encyclopedia_date ) ) ); ?></td>
				<td class="author column-author" data-colname="更新者"><?php echo esc_html( $author ); ?></td>
			</tr>
<?php endforeach; ?>
<?php else : ?>
			<tr class="no-items">
				<td class="colspanchange" colspan="4">記者記事が見つかりませんでした。</td>
			</tr>
<?php endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<th scope="col" class="manage-column column-name">名前（紐づいている病名）</th>
				<th scope="col" class="manage-column column-status">ステータス</th>
				<th scope="col" class="manage-column column-updated_date">更新日</th>
				<th scope="col" class="manage-column column-author">更新者</th>
			</tr>
		</tfoot>
	</table>
	<div class="tablenav bottom">
		<?php echo yomidr_admin_pager ( $paged, $max_pages, $found_rows, true ); ?>
		<br class="clear">
	</div>
	<div id="ajax-response"></div>
	<br class="clear">
</div>

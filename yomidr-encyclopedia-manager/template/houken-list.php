<?php if ( ! defined( 'ABSPATH' ) ) { exit; }
global $wpdb;

// 検索キーワードの取得
$search = isset( $_GET['s'] ) ? wp_unslash($_GET['s']) : '';
// ページ名の取得
$page_name = isset( $_GET['page'] ) ? wp_unslash($_GET['page']) : '';
// 1ページあたりの表示件数
$page_items = 10;
// 現在のページ番号
$paged = isset( $_GET['paged'] ) && (int)$_GET['paged'] ? (int)$_GET['paged'] : 1;
// SQLでの表示開始位置
$start = ( $paged - 1 ) * $page_items;

// SQLの作成
// 検索結果に応じて実行SQLを変更
// houkenテーブルのdisease_idがtermテーブルに存在しない場合、検索用と同じSQLにすると通常の一覧に表示されないため、SQL自体を変更
if ( $search ) {
	$sql = "
SELECT
 SQL_CALC_FOUND_ROWS
 h.houken_id,
 t.name,
 tm.meta_value,
 h.houken_content,
 h.houken_status,
 h.houken_date,
 h.houken_author
 FROM ({$this->houken_table} as h INNER JOIN {$wpdb->terms} as t ON h.disease_slug = t.slug)
 INNER JOIN {$wpdb->termmeta} as tm ON t.term_id = tm.term_id
 WHERE tm.meta_key = 'disease_kana' AND (t.name LIKE '%%%s%%' OR tm.meta_value LIKE '%%%s%%') LIMIT %d, %d;";
	$houkens = $wpdb->get_results( $wpdb->prepare( $sql, $search, $search, $start, $page_items  ) );
} else {
	$sql = "
SELECT
 SQL_CALC_FOUND_ROWS
 houken_id,
 disease_slug,
 houken_content,
 houken_status,
 houken_date,
 houken_author
 FROM {$this->houken_table}
 LIMIT %d, %d;";
	$houkens = $wpdb->get_results( $wpdb->prepare( $sql, $start, $page_items  ) );
}

// 取得結果に応じて変数を設定
if ( $houkens ) {
	$found_rows = $wpdb->get_var( 'SELECT FOUND_ROWS()' );
	$max_pages = ceil( $found_rows / $page_items );
} else {
	$found_rows = 0;
	$max_pages = 0;
}
?>
<div class="wrap">
	<h1>法研</h1>
	<form id="houken-filter" method="get">
		<input type="hidden" name="page" value="<?php echo esc_attr( $page_name ); ?>">
		<p class="search-box">
			<label class="screen-reader-text" for="post-search-input">法研を検索:</label>
			<input id="post-search-input" type="search" value="" name="s">
			<input id="search-submit" class="button" type="submit" value="法研を検索">
		</p>
	</form>
	<?php echo yomidr_admin_pager ( $paged, $max_pages, $found_rows ); ?>
	<table class="wp-list-table widefat fixed striped houken">
		<thead>
			<tr>
				<th id="disease_name" class="manage-column column-disease_name" scope="col">名前（紐づいている病名）</th>
				<th id="houken_status" class="manage-column column-houken_status" scope="col">ステータス</th>
				<th id="houken_date" class="manage-column column-houken_date" scope="col">更新日</th>
				<th id="houken_author" class="manage-column column-houken_author" scope="col">更新者</th>
			</tr>
		</thead>
		<tbody id="the-list">
<?php
if ( $houkens ) :
	foreach( $houkens as $houken ) :
		$user = get_userdata($houken->houken_author);
		if ( $user && $user->display_name ) {
			$author = $user->display_name;
		} else {
			$author = '削除済みユーザー';
		}

		// 通常時と検索時で病名の取得方法が異なる
		if ( $search ) {
			$disease_name = $houken->name;
		} else {
			$disease = get_term_by( 'slug', $houken->disease_slug, 'disease' );
			if ( $disease ) {
				$disease_name = $disease->name;
			} else {
				$disease_name = '（病名がありません）';
			}
		}
?>
			<tr id="houken-<?php echo esc_attr( $houken->houken_id ); ?>" class="iedit author-self level-0 houken-<?php echo esc_attr( $houken->houken_id ); ?> type-houken status-publish format-standard hentry">
				<td class="title column-title has-row-actions column-primary page-title" data-colname="名前（紐づいている病名）">
					<strong>
						<a class="row-title" title="“<?php echo esc_attr( $disease_name ); ?>”を編集する" href="<?php echo esc_url( admin_url( 'admin.php?page=encyclopedia-manager.phpyomidr-hoken-list&houken='. esc_attr( $houken->houken_id ).'&action=edit' ) ); ?>"><?php echo esc_html( $disease_name ); ?></a>
					</strong>
				</td>
				<td class="status column-status" data-colname="ステータス"><?php echo esc_html( $houken->houken_status ? '公開' : '非公開' ); ?></td>
				<td class="date column-date" data-colname="更新日"><?php echo esc_html( date_i18n( get_option('date_format').' H:i', strtotime( $houken->houken_date ) ) ); ?></td>
				<td class="author column-author" data-colname="更新者"><?php echo esc_html( $author ); ?></td>
			</tr>
<?php endforeach; ?>
<?php else : ?>
			<tr class="no-items">
				<td class="colspanchange" colspan="4">法研記事が見つかりませんでした。</td>
			</tr>
<?php endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<th class="manage-column column-disease_name" scope="col">名前（紐づいている病名）</th>
				<th class="manage-column column-houken_status" scope="col">ステータス</th>
				<th class="manage-column column-houken_date" scope="col">更新日</th>
				<th class="manage-column column-houken_author" scope="col">更新者</th>
			</tr>
		</tfoot>
	</table>
	<?php echo yomidr_admin_pager ( $paged, $max_pages, $found_rows, true ); ?>
</div>

jQuery(function($) {

	// バリデーション（法研）
	if($("#form-houken").length) {
		$("#form-houken").validate({
			errorLabelContainer: "#errorList",
			wrapper: "li",
			messages: {
				houken_status: {
					required: "「ステータス」を選択してください。"
				},
				disease_slug: {
					required: "「病名」を選択してください"
				},
				houken_content: {
					required: "「記事内容」を入力してください。"
				}
			},
			rules: {
				houken_status: {
					required: true
				},
				disease_slug: {
					required: true
				},
				houken_content: {
					required: true
				}
			}
		});
	}

	// バリデーション（記者記事）
	if($("#form-encyclopedia").length) {
		$("#form-encyclopedia").validate({
			errorLabelContainer: "#errorList",
			wrapper: "li",
			messages: {
				encyclopedia_status: {
					required: "「ステータス」を選択してください。"
				},
				disease_slug: {
					required: "「病名」を選択してください"
				},
				encyclopedia_content: {
					required: "「記事内容」を入力してください。"
				}
			},
			rules: {
				encyclopedia_status: {
					required: true
				},
				disease_slug: {
					required: true
				},
				encyclopedia_content: {
					required: true
				}
			}
		});
	}

	// 部位・テーマで病名を絞り込む
	var part = $("[name=part]");
	part.change(function() {
		var part_selected = part.find("option:selected");
		var disease_slug = part_selected.val();
		if ( disease_slug == -1 ) {
			$("[name=disease_slug]").parents("li").show();
		} else {
			$("[name=disease_slug]").parents("li").hide();
			$("." + disease_slug).show();
		}
	});

	// 現在選択されている病名テキストを出力する
	var checked_disease_input = $("[name=disease_slug]");
	var checked_disease_text = $("#checked_disease_text .checked_disease_text_target");
	var checked_disease_name = $("[name=disease_slug]:checked").next(".checked_disease_target").text();
	
	if (checked_disease_name != "") {
		checked_disease_text.text(checked_disease_name);	
	} else {
		checked_disease_text.text("なし");	
	}

	function set_disease_text() {
		checked_disease_input.each(function() {
			if ($(this).is(":checked")) {
				checked_disease_name = $(this).next(".checked_disease_target").text();
				checked_disease_text.text(checked_disease_name);
			}
		});
	}

	checked_disease_input.change(function() {
		set_disease_text();
	});


	// 「削除」のポップアップウィンドウ
	if ($("#delete").length) {
		var delete_button = $("#delete");
		delete_button.click(function() {
			var ret = confirm("削除を行います。本当によろしいですか。");
			if (!ret) {
				return false;
			}
		});
	}
});


<?php
/*
Plugin Name: yomiDr. Encyclopedia Manager
Description: 法研と記者記事を管理します。
Author: Prime Strategy Co.,LTD.
Version: 1.0.0
Author URI: http://www.prime-strategy.co.jp/
*/

Class yomiDr_Encyclopedia_Manager {
	// 権限関係
	private $manage_all_cap = 'administrator';
	private $manage_houken_cap = array(
		'administrator',
		'editor'
	);

	private $manage_encyclopedia_cap = array(
		'administrator',
		'editor',
		'contributor'
	);

	private $no_manage_cap = 'no_manage_cap';

	// その他
	private $version = '1.0.0';
	private $plugin_basename;
	private $plugin_dir;
	private $plugin_dir_url;
	private $template_dir;
	private $css_dir;
	private $js_dir;
	private $encyclopedia_history_log;

	// テーブル名
	private $houken_table;
	private $encyclopedia_table;

	// テーブルカラム
	private $houken_primary_key = 'houken_id';
	private $houken_columns = array(
		'disease_slug'     => array( 'format' => '%s', 'name' => '病名スラッグ', 'validate' => array( 'required' ) ),
		'houken_content' => array( 'format' => '%s', 'name' => '法研記事',       'validate' => array( 'required' ) ),
		'houken_status'  => array( 'format' => '%s', 'name' => '法研ステータス', 'validate' => array( 'required' ) ),
		'houken_date'    => array( 'format' => '%s', 'name' => '更新日',         'validate' => array() ),
		'houken_author'  => array( 'format' => '%d', 'name' => '更新者',         'validate' => array() ),
	);
	private $encyclopedia_primary_key = 'encyclopedia_id';
	private $encyclopedia_columns = array(
		'disease_slug'           => array( 'format' => '%s', 'name' => '病名スラッグ',     'validate' => array( 'required' ) ),
		'encyclopedia_content' => array( 'format' => '%s', 'name' => '記事',           'validate' => array( 'required' ) ),
		'encyclopedia_status'  => array( 'format' => '%s', 'name' => '記事ステータス', 'validate' => array( 'required' ) ),
		'encyclopedia_date'    => array( 'format' => '%s', 'name' => '更新日',         'validate' => array() ),
		'encyclopedia_author'  => array( 'format' => '%d', 'name' => '更新者',         'validate' => array() ),
	);

	// エラーメッセージ
	private $error_messages = array(
		'required' => '必須項目です。',
	);

	public function __construct() {
		global $wpdb;

		$this->plugin_basename = plugin_basename( __FILE__ );
		$this->plugin_dir      = plugin_dir_path( __FILE__ );
		$this->plugin_dir_url  = plugin_dir_url( __FILE__ );
		$this->template_dir    = $this->plugin_dir . 'template/';
		$this->css_dir         = $this->plugin_dir_url . 'css/';
		$this->js_dir          = $this->plugin_dir_url . 'js/';
		$this->encyclopedia_history_log = 'encyclopedia_history_'.date( 'Y_m' ).'.log';

		$this->houken_table       = $wpdb->prefix . 'houken';
		$this->encyclopedia_table = $wpdb->prefix . 'encyclopedia';

		// Add Encyclopedia Manage Menu
		add_action( 'admin_menu', array( $this, 'add_manage_menu' ) );
	}

	/**
	* 管理画面に症状のメニューを追加する
	*
	* @access public
	* @since  1.0.0
	* @return なし
	*/
	public function add_manage_menu() {
		global $current_user;

		// 権限の判別
		if ( isset( $current_user->roles[0] ) && in_array( $current_user->roles[0], $this->manage_encyclopedia_cap ) ) {
			$this->manage_all_cap = $current_user->roles[0];
		} else {
			$this->manage_all_cap = $this->no_manage_cap;
		}

		add_submenu_page( $this->plugin_basename, '記者記事一覧', '記者記事一覧', $this->manage_all_cap, __FILE__, array( $this, 'encyclopedia_manage_page' ) );

		$slug = add_menu_page  ( '医療大全', '医療大全', $this->manage_all_cap, __FILE__, array( $this, 'encyclopedia_manage_page' ), '', 6 );

		add_action( 'load-' . $slug, array( $this, 'update_encyclopedia' ) );
		add_action( 'load-' . $slug, array( $this, 'enqueue' ) );

		$slug = add_submenu_page( $this->plugin_basename, '記者記事新規追加', '記者記事新規追加', $this->manage_all_cap, basename( __FILE__ ) . 'yomidr-encyclopedia-add', array( $this, 'encyclopedia_add_page' ) );
		add_action( 'load-' . $slug, array( $this, 'add_encyclopedia' ) );
		add_action( 'load-' . $slug, array( $this, 'enqueue' ) );

		// 権限の判別
		if ( isset( $current_user->roles[0] ) && in_array( $current_user->roles[0], $this->manage_houken_cap ) ) {
			$this->manage_all_cap = $current_user->roles[0];
		} else {
			$this->manage_all_cap = $this->no_manage_cap;
		}

		$slug = add_submenu_page( $this->plugin_basename, '法研一覧', '法研一覧', $this->manage_all_cap, basename( __FILE__ ) . 'yomidr-hoken-list', array( $this, 'houken_manage_page' ) );
		add_action( 'load-' . $slug, array( $this, 'update_houken' ) );
		add_action( 'load-' . $slug, array( $this, 'enqueue' ) );

	}

	/**
	 * 管理画面で読み込むCSS、jsをキューに登録する
	 *
	 * @access public
	 * @since  1.0.0
	 * @return なし
	 */
	public function enqueue() {
		wp_enqueue_style( 'yomidr-encyclopedia', $this->css_dir . 'style.css', array(), $this->version );
		wp_enqueue_script( 'validate', '//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js', array(), '1.0.0' );
		wp_enqueue_script( 'validate-ja', '//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/localization/messages_ja.js', array(), '1.0.0' );
		wp_enqueue_script( 'yomidr-encyclopedia', $this->js_dir . 'script.js', array(), $this->version, true );
	}

	public function houken_manage_page() {
		$method = 'houken_list_page';
		if ( isset( $_GET['action'] ) ) {
			switch ( $_GET['action'] ) {
				case 'edit' :
					if ( isset( $_GET['houken'] ) ) {
						$method = 'houken_add_page';
					}
					break;
			}
		}
		$this->$method();
	}

	public function encyclopedia_manage_page() {
		$method = 'encyclopedia_list_page';
		if ( isset( $_GET['action'] ) ) {
			switch ( $_GET['action'] ) {
				case 'edit' :
					if ( isset( $_GET['encyclopedia'] ) ) {
						$method = 'encyclopedia_add_page';
					}
					break;
			}
		}
		$this->$method();
	}

	/**
	 * 管理画面の各ページテンプレートの読み込み
	 *
	 * @access public
	 * @since  1.0.0
	 * @return なし
	 */
	public function houken_list_page() {
		$template = $this->template_dir . 'houken-list.php';
		if ( file_exists( $template ) && is_readable( $template ) ) {
			include( $template );
		}
	}
	public function encyclopedia_list_page() {
		$template = $this->template_dir . 'encyclopedia-list.php';
		if ( file_exists( $template ) && is_readable( $template ) ) {
			include( $template );
		}
	}
	public function houken_add_page() {
		$template = $this->template_dir . 'houken-edit.php';
		if ( file_exists( $template ) && is_readable( $template ) ) {
			include( $template );
		}
	}
	public function encyclopedia_add_page() {
		$template = $this->template_dir . 'encyclopedia-edit.php';
		if ( file_exists( $template ) && is_readable( $template ) ) {
			include( $template );
		}
	}

	/**
	 * 記者記事情報を取得する
	 *
	 * @access public
	 * @param $encyclopedia_id
	 * @return object|false 記者記事情報
	 */
	public function get_encyclopedia( $encyclopedia_id ) {
		global $wpdb;
		$sql = $wpdb->prepare( "SELECT * FROM {$this->encyclopedia_table} WHERE `encyclopedia_id` = %d", $encyclopedia_id );
		$encyclopedia = $wpdb->get_row( $sql );
		return $encyclopedia;
	}

	/**
	 * 記者記事情報を病名スラッグから取得する
	 *
	 * @access public
	 * @param $disease_slug
	 * @return object|false 記者記事情報
	 */
	public function get_encyclopedia_by_disease( $disease_slug ) {
		global $wpdb;
		$sql = $wpdb->prepare( "SELECT * FROM {$this->encyclopedia_table} WHERE `disease_slug` = %s", $disease_slug );
		$encyclopedia = $wpdb->get_row( $sql );
		return $encyclopedia;
	}

	/**
	 * 法研情報を取得する
	 *
	 * @access public
	 * @param $houken_id
	 * @return object|false 法研情報
	 */
	public function get_houken( $houken_id ) {
		global $wpdb;
		$sql = $wpdb->prepare( "SELECT * FROM {$this->houken_table} WHERE `houken_id` = %d", $houken_id );
		$houken = $wpdb->get_row( $sql );
		return $houken;
	}

	/**
	 * 法研情報を病名スラッグから取得する
	 *
	 * @access public
	 * @param $disease_slug
	 * @return object|false 法研情報
	 */
	public function get_houken_by_disease( $disease_slug ) {
		global $wpdb;
		$sql = $wpdb->prepare( "SELECT * FROM {$this->houken_table} WHERE `disease_slug` = %s", $disease_slug );
		$houken = $wpdb->get_row( $sql );
		return $houken;
	}

	/**
	 * encyclopedia_count カラムの更新処理
	 *
	 * 任意の病名に紐づく法研および記者記事をカウントし、DB に保存する。
	 *
	 * @access public
	 * @param \WP_Term $disease 記事数をカウントしたい病名タームオブジェクト
	 * @return void
	 */
	public function populate_encyclopedia_count( $disease ) {
		global $wpdb;
		$disease_id = $disease->term_id;
		$disease_slug = $disease->slug;

		// 病名スラッグが $disease_slug かつ記事ステータスが公開:1の法研の数
		$count_from_houken = $wpdb->get_var(
			$wpdb->prepare( "
				SELECT COUNT(*)
				FROM {$this->houken_table}
				WHERE disease_slug = %s AND houken_status = 1
			", $disease_slug
			)
		);

		// 病名スラッグが $disease_slug かつ記事ステータスが公開:1の記者記事の数
		$count_from_encyclopedia = $wpdb->get_var(
			$wpdb->prepare( "
				SELECT COUNT(*)
				FROM {$this->encyclopedia_table}
				WHERE disease_slug = %s AND encyclopedia_status = 1
			", $disease_slug
			)
		);

		// $disease の encyclopedia_count を算出
		$count = $count_from_houken + $count_from_encyclopedia;

		// $disease の encyclopedia_count カラムを更新
		$wpdb->query(
			$wpdb->prepare( "
				UPDATE $wpdb->term_taxonomy
				SET encyclopedia_count = %d
				WHERE term_id = %d
				", $count, $disease_id
			)
		);
	}

	/**
	 * 法研の更新処理
	 *
	 * @access public
	 * @return なし
	 */
	public function update_houken() {
		global $wpdb, $current_user, $yomiDr_Disease_Manager;
		if ( isset( $_POST['edit_houken'] ) ) {
			$houken_id = $_GET['houken'];
			$houken = get_houken( $houken_id );
			$houken_status = $houken->houken_status;
			// 変更前病名スラッグ
			$disease_slug = $houken->disease_slug;
			$disease = get_term_by( 'slug', $disease_slug, 'disease' );
			check_admin_referer( 'edit-houken', 'edit-nonce' );
			$post_data = wp_unslash( $_POST );
			$data = $this->update_houken_data();
			// houken_date を更新
			$data[0]['houken_date'] = current_time( 'mysql' );
			$data[1]['houken_date'] = '%s';
			// houken_author を更新
			$data[0]['houken_author'] = $current_user->ID;
			$data[1]['houken_author'] = '%d';

			// 変更後病名スラッグ
			$disease_slug_after = $data[0]['disease_slug'];
			$disease_after = get_term_by( 'slug', $data[0]['disease_slug'], 'disease' );

			$wpdb->update(
				$this->houken_table,
				$data[0],
				array( 'houken_id' => $houken_id ),
				$data[1],
				array( '%d' )
			);

			// ログに記録
			$message = "[" . date( 'D M d H:i:s Y', current_time( 'timestamp' ) ) . "] [history] Edit houken_id {$houken_id} by author {$current_user->ID}";
			$yomiDr_Disease_Manager->logging( $this->encyclopedia_history_log, $message );

			// ステータスか病名いずれかが変更されたら encyclopedia_count カラムを更新
			if ( $houken_status != $data[0]['houken_status'] || $disease_slug != $disease_slug_after ) {
				// 変更前の病名の encyclopedia_count を更新
				$this->populate_encyclopedia_count( $disease );
				// 変更後の病名の encyclopedia_count を更新
				$this->populate_encyclopedia_count( $disease_after );
			}
			$redirect_to = admin_url( 'admin.php?page=encyclopedia-manager.phpyomidr-hoken-list&houken=' . $houken_id . '&action=edit&message=3' );
			wp_redirect( $redirect_to );

		}
	}

	private function update_houken_data() {
		$data = array();
		$formats = array();
		$post_data = wp_unslash( $_POST );
		foreach ( $this->houken_columns as $column => $arr ) {
			if ( isset( $post_data[$column] ) ) {
				if ( is_array( $post_data[$column] ) ) {
					$post_data[$column] = array_map( 'trim', $post_data[$column] );
				} else {
					$post_data[$column] = trim( $post_data[$column] );
				}
				$data[$column] = $post_data[$column];
				$formats[] = $arr['format'];
			}
		}
		return array( $data, $formats );
	}

	/**
	 * 記者記事の更新・削除処理
	 *
	 * @access public
	 * @return なし
	 */
	public function update_encyclopedia() {

		global $wpdb, $current_user, $yomiDr_Disease_Manager;

		if ( isset( $_GET['action'] ) && 'delete' == $_GET['action'] && isset( $_GET['encyclopedia'] ) ) {
			$encyclopedia_id = $_GET['encyclopedia'];
			$encyclopedia = get_encyclopedia( $encyclopedia_id );
			$encyclopedia_status = $encyclopedia->encyclopedia_status;
			$disease_slug = $encyclopedia->disease_slug;
			$disease = get_term_by( 'slug', $disease_slug, 'disease' );
			if ( ! isset( $_GET['nonce'] ) || ! wp_verify_nonce( $_GET['nonce'], 'delete-encyclopedia' ) ) {
				wp_die( 'リンクの有効期限切れです' );
			}
			$wpdb->delete( $this->encyclopedia_table, array( 'encyclopedia_id' => $_GET['encyclopedia'] ) );

			// ログに記録
			$message = "[" . date( 'D M d H:i:s Y', current_time( 'timestamp' ) ) . "] [history] Delete encyclopedia_id {$encyclopedia_id} by author {$current_user->ID}";
			$yomiDr_Disease_Manager->logging( $this->encyclopedia_history_log, $message );

			// encyclopedia_count カラムを更新
			$this->populate_encyclopedia_count( $disease );

			$redirect_to = admin_url( 'admin.php?page=yomidr-encyclopedia-manager%2Fencyclopedia-manager.php' );
			wp_redirect( $redirect_to );
			exit;

		} elseif ( isset( $_POST['edit_encyclopedia'] ) ) {
			$encyclopedia_id = $_GET['encyclopedia'];
			$encyclopedia = get_encyclopedia( $encyclopedia_id );
			$encyclopedia_status = $encyclopedia->encyclopedia_status;
			$disease_slug = $encyclopedia->disease_slug;
			$disease = get_term_by( 'slug', $disease_slug, 'disease' );
			check_admin_referer( 'edit-encyclopedia', 'edit-nonce' );
			$post_data = wp_unslash( $_POST );
			$data = $this->update_encyclopedia_data();
			// encyclopedia_date を更新
			$data[0]['encyclopedia_date'] = current_time( 'mysql' );
			$data[1]['encyclopedia_date'] = '%s';
			// encyclopedia_author を更新
			$data[0]['encyclopedia_author'] = $current_user->ID;
			$data[1]['encyclopedia_author'] = '%d';

			$disease_slug_after = $data[0]['disease_slug'];
			$disease_after = get_term_by( 'slug', $data[0]['disease_slug'], 'disease' );

			$wpdb->update( $this->encyclopedia_table, $data[0], array( 'encyclopedia_id' => $_GET['encyclopedia'] ), $data[1], array( '%d' ) );

			// ログに記録
			$message = "[" . date( 'D M d H:i:s Y', current_time( 'timestamp' ) ) . "] [history] Edit encyclopedia_id {$encyclopedia_id} by author {$current_user->ID}";
			$yomiDr_Disease_Manager->logging( $this->encyclopedia_history_log, $message );

			// ステータスか病名いずれかが変更されたら encyclopedia_count カラムを更新
			if ( $encyclopedia_status != $data[0]['encyclopedia_status'] || $disease_slug != $disease_slug_after ) {
				// 変更前の病名の encyclopedia_count を更新
				$this->populate_encyclopedia_count( $disease );
				// 変更後の病名の encyclopedia_count を更新
				$this->populate_encyclopedia_count( $disease_after );
			}

			$redirect_to = admin_url( 'admin.php?page=yomidr-encyclopedia-manager%2Fencyclopedia-manager.php&encyclopedia=' . $encyclopedia_id . '&action=edit&message=3' );
			wp_redirect( $redirect_to );

		}
	}

	/**
	 * 記者記事の追加処理
	 *
	 * @access public
	 * @return なし
	 */
	public function add_encyclopedia() {
		global $wpdb, $current_user, $yomiDr_Disease_Manager;
		if ( isset( $_POST['add_encyclopedia'] ) ) {
			check_admin_referer( 'add-encyclopedia', 'add-nonce' );
			$post_data = wp_unslash( $_POST );
			$data = $this->create_encyclopedia_data();

			// encyclopedia_date を配列に追加
			$data[0]['encyclopedia_date'] = current_time( 'mysql' );
			$data[1]['encyclopedia_date'] = '%s';
			// encyclopedia_author を配列に追加
			$data[0]['encyclopedia_author'] = $current_user->ID;
			$data[1]['encyclopedia_author'] = '%d';

			$ret = $wpdb->insert( $this->encyclopedia_table, $data[0], $data[1] );

			// ログに記録
			$message = "[" . date( 'D M d H:i:s Y', current_time( 'timestamp' ) ) . "] [history] Create encyclopedia_id {$wpdb->insert_id} by author {$current_user->ID}";
			$yomiDr_Disease_Manager->logging( $this->encyclopedia_history_log, $message );

			// encyclopedia_count カラムを更新
			$disease_slug = $data[0]['disease_slug'];
			$disease = get_term_by( 'slug', $disease_slug, 'disease' );
			$this->populate_encyclopedia_count( $disease );

			// リダイレクト
			$edit_link = add_query_arg( array( 'action' => 'edit', 'encyclopedia' => $wpdb->insert_id, 'message' => 1 ), admin_url( 'admin.php?page=yomidr-encyclopedia-manager%2Fencyclopedia-manager.php' ) );
			wp_redirect( $edit_link );
			exit;
		}
	}

	private function create_encyclopedia_data() {
		$data = array();
		$formats = array();
		// encyclopedia_id を配列に追加
		$data['encyclopedia_id'] = '';
		$formats[] = '%d';
		$post_data = wp_unslash( $_POST );
		foreach ( $this->encyclopedia_columns as $column => $arr ) {
			if ( isset( $post_data[$column] ) ) {
				if ( is_array( $post_data[$column] ) ) {
					$post_data[$column] = array_map( 'trim', $post_data[$column] );
				} else {
					$post_data[$column] = trim( $post_data[$column] );
				}
				$data[$column] = $post_data[$column];
				$formats[] = $arr['format'];
			}
		}
		return array( $data, $formats );

	}

	private function update_encyclopedia_data() {
		$data = array();
		$formats = array();
		$post_data = wp_unslash( $_POST );
		foreach ( $this->encyclopedia_columns as $column => $arr ) {
			if ( isset( $post_data[$column] ) ) {
				if ( is_array( $post_data[$column] ) ) {
					$post_data[$column] = array_map( 'trim', $post_data[$column] );
				} else {
					$post_data[$column] = trim( $post_data[$column] );
				}
				$data[$column] = $post_data[$column];
				$formats[] = $arr['format'];
			}
		}
		return array( $data, $formats );
	}

} // class end.
$yomiDr_Encyclopedia_Manager = new yomiDr_Encyclopedia_Manager;

// load global functions
require_once( __DIR__ . '/inc/functions.php' );

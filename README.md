# 概要・導入

* ヨミドク開発で、wp_term_taxonomy テーブルに、encyclopedia_count カラムを追加した
* 医療大全の法研と記者記事は、カスタムタクソノミー「病名」に紐づく
* 法研・記者記事の更新時（削除なども含め）、「病名」の encyclopedia_count カラムに、その病名に紐づいている、公開状態の法研と記者記事のカウント数（総数）が入るように実装する

※今回のコードレビューでは、記者記事の更新時について取り扱う



# 実装・コード

## 記者記事新規追加における encyclopedia_count カラムの更新

/yomidr-encyclopedia-manager/encyclopedia-manager.php Line 558 ~ 589

🔗　https://bitbucket.org/prime-strategy/review-2016-03-04/src/f48e0d5afa24abed45294a9d9e2b41fcdde34c4d/yomidr-encyclopedia-manager/encyclopedia-manager.php?at=master&fileviewer=file-view-default#encyclopedia-manager.php-558:589


## 記者記事更新における encyclopedia_count カラムの更新

/yomidr-encyclopedia-manager/encyclopedia-manager.php Line 465 ~ 524

🔗　https://bitbucket.org/prime-strategy/review-2016-03-04/src/f48e0d5afa24abed45294a9d9e2b41fcdde34c4d/yomidr-encyclopedia-manager/encyclopedia-manager.php?at=master&fileviewer=file-view-default#encyclopedia-manager.php-465:524


## 記者記事削除における encyclopedia_count カラムの更新

/yomidr-encyclopedia-manager/encyclopedia-manager.php Line 406 ~ 432

🔗　https://bitbucket.org/prime-strategy/review-2016-03-04/src/f48e0d5afa24abed45294a9d9e2b41fcdde34c4d/yomidr-encyclopedia-manager/encyclopedia-manager.php?at=master&fileviewer=file-view-default#encyclopedia-manager.php-406:432



# 気になった点

* encyclopedia_count カラムの実装について、よりよい実現方法があれば、助言をいただきたい
* よりシンプルにコードを書きたい